package com.kshrd.hw02;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class NotebookActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notebook);

        listView = findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);

    }

    public void onClickAdd(View v) {
        Intent intent = new Intent();
        intent.setClass(NotebookActivity.this, NoteAddActivity.class);
        startActivityForResult(intent, IntentConstraints.INTENT_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IntentConstraints.INTENT_REQUEST_CODE) {
            assert data != null;
            title = data.getStringExtra(IntentConstraints.INTENT_MESSAGE_CODE);
            arrayList.add(title);
            arrayAdapter.notifyDataSetChanged();
        }
    }
}