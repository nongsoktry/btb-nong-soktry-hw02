package com.kshrd.hw02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class NoteAddActivity extends AppCompatActivity {

    EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_add);

        inputText = findViewById(R.id.edTitle);
    }

    public void onClickAdd(View view) {
        String title = inputText.getText().toString();
        if (title.isEmpty()) {
            Toast.makeText(this, "Please input something!", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent();
            intent.putExtra(IntentConstraints.INTENT_MESSAGE_CODE, title);
            setResult(IntentConstraints.INTENT_RESULT_CODE, intent);
            finish();
        }
    }

    public void onClickCancel(View view) {
        Intent intent = new Intent(this, NotebookActivity.class);
        startActivity(intent);
    }
}